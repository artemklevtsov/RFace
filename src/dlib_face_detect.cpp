// [[Rcpp::plugins(cpp11)]]

#include <Rcpp.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing.h>
#include "utils.h"
#include "wrap.h"

using namespace Rcpp;
using namespace dlib;


// [[Rcpp::export]]
SEXP frontal_face_detector(SEXP img) {
    // Convert cv::Mat to dlib::matrix
    array2d<unsigned char> dimg = get_dlib_image(img);

    // Detect faces
    dlib::frontal_face_detector face_detector = dlib::get_frontal_face_detector();
    // dlib::pyramid_up(img);
    std::vector<dlib::rectangle> faces = face_detector(dimg, 1);

    return wrap(faces);
}
