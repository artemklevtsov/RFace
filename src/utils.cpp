#include <Rcpp.h>
#include <opencv2/opencv.hpp>
#include <dlib/opencv/cv_image.h>
#include <dlib/image_processing.h>
#include "utils.h"

using namespace Rcpp;
using namespace dlib;
using namespace cv;

array2d<unsigned char> get_dlib_image(SEXP obj) {
    // Get cv::Mat from XPtr
    XPtr<cv::Mat> ptr(obj);
    cv::Mat cvimg = *ptr.get();

    // Convert to gray scale
    cv::Mat gray;
    if (cvimg.channels() != 1) {
        cvtColor(cvimg, gray, COLOR_BGR2GRAY);
    } else {
        gray = cvimg;
    }

    // Convert cv::Mat to dlib::matrix
    array2d<unsigned char> dimg;
    assign_image(dimg, cv_image<unsigned char>(gray));
    return dimg;
}
