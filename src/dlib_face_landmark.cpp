// [[Rcpp::plugins(cpp11)]]

#include <Rcpp.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing.h>
#include "utils.h"

using namespace Rcpp;
using namespace dlib;


// [[Rcpp::export]]
List dlib_face_landmark(SEXP img, const List& faces, std::string model) {
    // Convert cv::Mat to dlib::matrix
    array2d<unsigned char> dimg = get_dlib_image(img);

    // Get shape predictor
    shape_predictor sp;
    deserialize(model) >> sp;
    // Get sizes
    size_t n_faces = faces.size();
    size_t n_points = sp.num_parts();

    List res = no_init(n_faces);
    for (size_t i = 0; i < n_faces; ++i) {
        // Get face landmark points
        IntegerVector face = faces[i];
        dlib::rectangle rect(face[0], face[3], face[2], face[1]);
        full_object_detection lm = sp(dimg, rect);
        // Extract points
        IntegerVector x = no_init(n_points);
        IntegerVector y = no_init(n_points);
        for (size_t j = 0; j < n_points ; ++j) {
            dlib::point p = lm.part(j);
            x[j] = p.x();
            y[j] = p.y();
        }
        List tmp = List::create(x, y);
        tmp.attr("names") = CharacterVector::create("x", "y");
        res[i] = tmp;
    }

    res.attr("class") = "dlib-landmarks";
    res.attr("faces") = faces.size();

    return res;
}
