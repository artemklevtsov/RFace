#ifndef WRAP_H
#define WRAP_H

#include <RcppCommon.h>
#include <dlib/image_processing.h>


namespace Rcpp {
    SEXP wrap(const dlib::rectangle& x);
    SEXP wrap(const std::vector<dlib::rectangle>& x);
}

#include <Rcpp.h>

namespace Rcpp {

    SEXP wrap(const dlib::rectangle& x) {
        IntegerVector res = IntegerVector::create(
            x.left(),
            x.bottom(),
            x.right(),
            x.top()
        );
        res.attr("names") =  CharacterVector::create("left", "bottom", "right", "top");
        return res;
    }

    SEXP wrap(const std::vector<dlib::rectangle>& x) {
        size_t n = x.size();
        IntegerVector faces = no_init(n);
        IntegerVector left = no_init(n);
        IntegerVector bottom = no_init(n);
        IntegerVector right = no_init(n);
        IntegerVector top = no_init(n);
        for (size_t i = 0; i < n; ++i) {
            dlib::rectangle rect = x[i];
            faces[i] = i;
            left[i] = rect.left();
            bottom[i] = rect.bottom();
            right[i] = rect.right();
            top[i] = rect.top();
        }
        DataFrame res = DataFrame::create(
            Named("face") = faces,
            Named("left") = left,
            Named("bottom") = bottom,
            Named("right") = right,
            Named("top") = top
        );
        return res;
    }

}


#endif
