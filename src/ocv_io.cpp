// [[Rcpp::plugins(cpp11)]]

#include <Rcpp.h>
#include <opencv2/opencv.hpp>

using namespace Rcpp;
using namespace cv;


// [[Rcpp::export]]
SEXP ocv_read(std::string file, double limit) {
    // Read imgae
    cv::Mat img = cv::imread(file);
    if (img.empty()){
        throw std::runtime_error("Can not read image.");
    }

    // Auto scale to limit
    if (limit != R_PosInf) {
        double max_side = std::max(img.cols, img.rows);
        double scale = max_side > limit ? (double) limit / max_side : 1.0;
        resize(img, img, cv::Size(), scale, scale, INTER_AREA);
    }

    XPtr<cv::Mat> res(new cv::Mat(img), true);
    res.attr("class") = "ocv-image";
    res.attr("width") = img.cols;
    res.attr("height") = img.rows;
    res.attr("channels") = img.channels();
    res.attr("size") = img.total() * img.elemSize();
    res.attr("path") = file;
    return res;
}


// [[Rcpp::export]]
std::string ocv_write(SEXP obj, std::string file) {
    XPtr<cv::Mat> ptr(obj);
    cv::Mat img = *ptr.get();
    cv::imwrite(file, img);
    return file;
}
