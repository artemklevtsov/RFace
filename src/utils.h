#ifndef UTILS_H
#define UTILS_H

#include <opencv2/opencv.hpp>
#include <dlib/opencv/cv_image.h>
#include <dlib/image_processing.h>

dlib::array2d<unsigned char> get_dlib_image(SEXP obj);

#endif
